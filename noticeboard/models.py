from django.db import models

# Create your models here.

class Notice(models.Model):
    title = models.CharField(max_length=200)
    text = models.TextField()
    contact_number = models.IntegerField()    

    def _str_(self):
        return f'Notice:{self.tile}:{self.text}:{self.contact_number}'